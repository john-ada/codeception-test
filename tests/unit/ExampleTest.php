<?php


class ExampleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function getFullName($firstName, $lastName) {
        return $firstName . " " . $lastName;
    }

    // tests
    public function testUserReturnsCorrectFullName()
    {
        $this->assertTrue($this->getFullName('John Paul', 'Ada') == 'John Paul Ada');
    }
}
